const { app, BrowserWindow, Menu, dialog, autoUpdater, ipcRenderer, ipcMain, remote } = require("electron")
const { Deeplink } = require("electron-deeplink")
const isDev = require("electron-is-dev")
const Store = require('./store.js')
const url = require("url")

function createWindow() {
	// Create the browser window.
	const mainWindow = new BrowserWindow({
		width: 250,
		height: 330,
		icon: "./Spotini.ico",
		resizable: false,
		frame: false,
		titleBarStyle: "hidden",
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false
		},
	})

	// and load the index.html of the app.
	mainWindow.loadFile("out/index.html")

	// Open the DevTools.
	if (isDev) {
		mainWindow.webContents.openDevTools()
	}

	return mainWindow
}

const store = new Store({
	configName: "user-preferences",
	defaults: {
		alwaysOnTop: false,
		refreshToken: null,
	}
})

app.whenReady().then(() => {
	const mainWindow = createWindow()

	// set up settings
	mainWindow.setAlwaysOnTop(store.get("alwaysOnTop"), "pop-up-menu")

	const protocol = "spotini"
	const deeplink = new Deeplink({ app, mainWindow, protocol, isDev })

	deeplink.on("received", (link) => {
		let route = url.parse(link).host

		if (route = "spotify-user-logged") {
			let startOfLink = protocol + "://" + url.parse(link).host + "/?code="
			let token = link.substring(startOfLink.length)

			console.log(token)
			mainWindow.webContents.send("spotify-user-logged", token)
		}
	})

	ipcMain.on("minimize", () => {
		mainWindow.minimize()
	})

	ipcMain.on("quit", () => {
		if (isDev) {
			mainWindow.webContents.closeDevTools()
		}
		mainWindow.close()
		app.quit()
	})

	ipcMain.on("toggle-always-on-top", () => {
		store.set("alwaysOnTop", !mainWindow.isAlwaysOnTop())
		mainWindow.setAlwaysOnTop(!mainWindow.isAlwaysOnTop(), "pop-up-menu")
	})

	ipcMain.on("set-refresh-token", (_evt, refreshToken) => {
		store.set("refreshToken", refreshToken)
	})

	ipcMain.on("get-refresh-token", () => {
		mainWindow.webContents.send("refresh-token", store.get("refreshToken"))
	})

	ipcMain.on("get-is-always-on-top", () => {
		mainWindow.webContents.send("is-always-on-top", mainWindow.isAlwaysOnTop())
	})

	app.on("activate", function () {
		// On macOS it"s common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (BrowserWindow.getAllWindows().length === 0) createWindow()
	})
})

// Quit when all windows are closed, except on macOS. There, it"s common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
	if (process.platform !== "darwin") app.quit()
})

if (isDev) {
	try {
		require("electron-reloader")(module)
	} catch (_) { }
}

Menu.setApplicationMenu(null)