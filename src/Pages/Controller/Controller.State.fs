namespace Pages.Controller

open Elmish
open Fable.Core
open Fable.Core.JsInterop

module Cmds =
    let loadDevices accessToken =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result = API.Spotify.Controller.getDevices accessToken

                result
                |> Result.map (fun result -> result.devices)
                |> Msg.DevicesLoaded
                |> dispatch
            } |> Promise.start
        )

    let loadPlayer accessToken =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result = API.Spotify.Controller.getPlayer accessToken

                result
                |> Msg.PlayerLoaded
                |> dispatch
            } |> Promise.start
        )

    let setDevice accessToken deviceId =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! setDeviceResult = API.Spotify.Controller.setDevice accessToken deviceId

                do! Promise.sleep 300

                let! result =
                    match setDeviceResult with
                    | None -> API.Spotify.Controller.getPlayer accessToken
                    | Some error -> error |> Result.Error |> Promise.lift

                result
                |> Msg.PlaybackUpdated
                |> dispatch
            } |> Promise.start
        )

    let togglePlayPause isPlaying deviceId accessToken =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result =
                    if isPlaying then
                        API.Spotify.Controller.pauseSong deviceId accessToken
                    else
                        API.Spotify.Controller.resumeSong deviceId accessToken

                do! Promise.sleep 300

                let! result =
                    match result with
                    | Ok _ -> API.Spotify.Controller.getPlayer accessToken
                    | Error error -> error |> Result.Error |> Promise.lift

                result
                |> Msg.PlaybackUpdated
                |> dispatch
            } |> Promise.start
        )

module State =
    open Thoth.Fetch

    [<Import("osLocale", "os-locale")>]
    let osLocale : {| sync: unit -> unit |} = jsNative

    let init accessToken: Model * Cmd<Msg> =
        { AccessToken = accessToken
          FetchError = None
          CurrentPlayback = None
          Devices = None },
        Cmds.loadPlayer accessToken

    let fetchErrorTreat model error =
        match error with
        | NetworkError _
        | PreparingRequestFailed _ ->
            { model with FetchError = error |> Some },
            Cmd.none, None
        | DecodingFailed _response ->
            { model with FetchError = error |> Some },
            Cmd.none, None
        | FetchFailed _response ->
            { model with FetchError = error |> Some },
            Cmd.none, None

    let update msg model : Model * Cmd<Msg> * OutMsg option =
        match msg with
        | Msg.PlayerLoaded result ->
            match result with
            | Ok newPlaybackOpt ->
                { model with CurrentPlayback = newPlaybackOpt},
                Cmd.none, None
            | Error error -> fetchErrorTreat model error

        | Msg.PlaybackUpdated result ->
            match result with
            | Ok newPlaybackOpt ->
                { model with CurrentPlayback = newPlaybackOpt },
                Cmd.none, None
            | Error error -> fetchErrorTreat model error

        | Msg.LoadDevices ->
            model, Cmds.loadDevices model.AccessToken, None

        | Msg.DevicesLoaded result ->
            match result with
            | Ok devices ->
                { model with Devices = Some devices },
                Cmd.none, None
            | Error error -> fetchErrorTreat model error

        | Msg.SelectDevice device ->
            model,
            Cmds.setDevice model.AccessToken device.id,
            None

        | Msg.TogglePlayPause ->
            match model.CurrentPlayback with
            | Some currentPlayback ->
                model,
                Cmds.togglePlayPause
                    currentPlayback.is_playing
                    currentPlayback.device.id
                    model.AccessToken,
                None
            | None -> model, Cmd.none, None

        | Msg.NextSong ->
            printfn "next song"
            model, Cmd.none, None

        | Msg.PreviousSong ->
            printfn "previous song"
            model, Cmd.none, None

        | Msg.FetchError error ->
            printfn "%O" error

            model, Cmd.none, None