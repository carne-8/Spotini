namespace Pages.Controller

open Spotini

[<RequireQualifiedAccess>]
type Msg =
    | TogglePlayPause
    | NextSong
    | PreviousSong

    | PlayerLoaded of Result<Spotify.Playback option, Thoth.Fetch.FetchError>
    | PlaybackUpdated of Result<Spotify.Playback option, Thoth.Fetch.FetchError>

    | LoadDevices
    | DevicesLoaded of Result<Spotify.Device list, Thoth.Fetch.FetchError>

    | SelectDevice of Spotify.Device
    | FetchError of Thoth.Fetch.FetchError

[<RequireQualifiedAccess>]
type OutMsg =
    | RequestRefreshToken

type Model =
    { AccessToken: string
      FetchError: Thoth.Fetch.FetchError option
      CurrentPlayback: Spotify.Playback option
      Devices: Spotify.Device list option }