module Pages.Controller.View

open Feliz
open Elmish
open Spotini
open Spotini.Assets
open Fable.Core
open Fable.Core.JsInterop
open Zanaptak.TypedCssClasses

importAll "./Controller.sass"

[<Literal>]
let ControllerSass = __SOURCE_DIRECTORY__ + "/Controller.sass"
type Style = CssClasses<ControllerSass, Naming.Verbatim, commandFile = "node", argumentPrefix = "../sass-process.js">

type props =
    {| model: Model
       dispatch: Dispatch<Msg> |}

[<ReactComponent>]
let devicePopup model dispatch devicesPopupOpened setDevicesPopupOpened =
    React.fragment [
        if devicesPopupOpened then
            Html.div [
                prop.className Style.``popup-background``
                prop.onClick (fun _ ->
                    false |> setDevicesPopupOpened
                )
            ]

        Html.div [
            prop.classes [
                Style.``devices-popup``
                if devicesPopupOpened then
                    Style.``is-opened``
            ]
            prop.children [
                Html.div [
                    prop.className Style.``popup-description``
                    prop.text "Choose device to control"
                ]

                Html.div [
                    prop.className Style.``popup-content``
                    prop.children
                        (match model.Devices with
                        | Some devices when devices.Length > 0 ->
                            devices
                            |> List.map (fun device ->
                                Html.a [
                                    prop.key device.id
                                    prop.classes [
                                        Style.device

                                        match model.CurrentPlayback with
                                        | Some ({ device = currentDevice }) ->
                                            if currentDevice.id = device.id then
                                                Style.selected
                                        | _ -> ()
                                    ]
                                    prop.text device.name
                                    prop.onClick (fun _ ->
                                        false |> setDevicesPopupOpened

                                        device
                                        |> Msg.SelectDevice
                                        |> dispatch
                                    )
                                ]
                            )

                        | _ ->
                            [ Html.div [
                                prop.className Style.``device-empty``
                                prop.text I18n.pages.controller.noDevices
                            ]])
                ]
            ]
        ]
    ]

[<ReactComponent>]
let Controller (props: props) =
    let (devicesPopupOpened, setDevicesPopupOpened) = React.useState false

    Html.div [
        prop.className Style.page
        prop.children [
            match props.model.CurrentPlayback with
            | Some currentPlayback ->
                match currentPlayback.item with
                | Some song ->
                    Html.img [
                        prop.className Style.``song-image``
                        prop.src song.album.images.[0].url
                    ]
                | None ->
                    Html.div [
                        prop.text I18n.pages.controller.notConnected
                    ]
            | None ->
                Html.div [
                    prop.text I18n.pages.controller.notConnected
                ]

            Html.header [
                prop.children [
                    Html.i [
                        prop.classes [
                            Icons.``fi-rr-speaker``
                            GlobalCss.icon
                            GlobalCss.shadow
                            Style.``devices-icon``
                        ]
                        prop.onClick (fun _evt ->
                            if not devicesPopupOpened then
                                Msg.LoadDevices |> props.dispatch

                            true |> setDevicesPopupOpened
                        )
                    ]
                ]
            ]

            devicePopup
                props.model
                props.dispatch
                devicesPopupOpened
                setDevicesPopupOpened

            Html.footer [
                Html.i [
                    prop.classes [
                        GlobalCss.icon
                        Icons.``fi-rr-shuffle``
                        Style.shuffle
                    ]
                ]
                Html.div [
                    prop.className Style.``music-control-actions``
                    prop.children [
                        Html.i [
                            prop.classes [
                                GlobalCss.icon
                                Icons.``fi-rr-angle-left``
                            ]
                            prop.onClick (fun _evt ->
                                Msg.PreviousSong
                                |> props.dispatch
                            )
                        ]
                        Html.i [
                            prop.classes [
                                GlobalCss.icon
                                match props.model.CurrentPlayback with
                                | Some currentPlayback when currentPlayback.is_playing ->
                                    Icons.``fi-rr-pause``
                                | Some currentPlayback when not currentPlayback.is_playing ->
                                    Icons.``fi-rr-play``
                                | _ -> Icons.``fi-rr-play``
                            ]
                            prop.onClick (fun _evt ->
                                Msg.TogglePlayPause
                                |> props.dispatch
                            )
                        ]
                        Html.i [
                            prop.classes [
                                GlobalCss.icon
                                Icons.``fi-rr-angle-right``
                            ]
                            prop.onClick (fun _evt ->
                                Msg.NextSong
                                |> props.dispatch
                            )
                        ]
                    ]
                ]
                Html.i [
                    prop.classes [
                        GlobalCss.icon
                        Icons.``fi-rr-refresh``
                    ]
                ]
            ]
        ]
    ]