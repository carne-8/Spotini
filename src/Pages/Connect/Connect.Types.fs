namespace Pages.Connect

[<RequireQualifiedAccess>]
type Msg =
    | Connect

type Model =
    { AuthError: Thoth.Fetch.FetchError option }