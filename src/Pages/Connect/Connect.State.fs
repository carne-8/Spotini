namespace Pages.Connect

open Elmish
open Electron

module State =

    let init authError : Model * Cmd<Msg> =
        { AuthError = authError }, Cmd.none

    let update msg model : Model * Cmd<Msg> =
        match msg with
        | Msg.Connect ->
            let link = Env.get "SPOTIFY_LOGIN_URL"
            link |> electron.shell.openExternal |> ignore
            model, Cmd.none