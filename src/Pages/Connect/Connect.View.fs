module Pages.Connect.View

open Spotini
open Feliz
open Fable.Core.JsInterop
open Thoth.Fetch
open Zanaptak.TypedCssClasses

importAll "./Connect.sass"

[<Literal>]
let connectSass = __SOURCE_DIRECTORY__ + "/Connect.sass"
type Style = CssClasses<connectSass, Naming.Verbatim, commandFile = "node", argumentPrefix = "../sass-process.js">

let View model dispatch =
    React.fragment [
        Html.div [
            prop.className Style.page
            prop.children [
                match model.AuthError with
                | Some error ->
                    match error with
                    | DecodingFailed error ->
                        Html.p [
                            prop.classes [ "error-summary" ]
                            prop.text error
                        ]
                    | FetchFailed response ->
                        Html.p [
                            prop.classes [ "error-summary" ]
                            prop.text response.StatusText
                        ]
                        Html.p [
                            prop.classes [ "error-summary" ]
                            prop.text (I18n.warnings.simpleErrorTemplate response.StatusText)
                        ]
                    | NetworkError error ->
                        Html.p [
                            prop.classes [ "error-summary" ]
                            prop.text error.Message
                        ]
                    | PreparingRequestFailed error ->
                        Html.p [
                            prop.classes [ "error-summary" ]
                            prop.text error.Message
                        ]
                | None -> ()

                Html.div [
                    prop.className Style.``connect-button``
                    prop.onClick (fun _evt ->
                        Msg.Connect
                        |> dispatch
                    )
                    prop.text I18n.pages.connect.connect
                ]
            ]
        ]
    ]