module Spotini.Assets

open Zanaptak.TypedCssClasses

[<Literal>]
let controllerSass = __SOURCE_DIRECTORY__ + "/styles/global.sass"
type GlobalCss = CssClasses<controllerSass, Naming.Verbatim, commandFile = "node", argumentPrefix = "../sass-process.js">

[<Literal>]
let iconUrl = "https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css"
type Icons = CssClasses<iconUrl, Naming.Verbatim>

[<Literal>]
let iconBoldUrl = "https://cdn-uicons.flaticon.com/uicons-bold-rounded/css/uicons-bold-rounded.css"
type IconsBold = CssClasses<iconBoldUrl, Naming.Verbatim>