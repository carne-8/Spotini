module Main

open Feliz

open Elmish
open Elmish.React

open Elmish.Debug

open Browser.Dom
open Fable.Core.JsInterop

importSideEffects "./styles/global.sass"

let update msg model =
    let newModel, cmd = App.State.update msg model
    newModel, cmd

Program.mkProgram App.State.init update App.View.view
|> Program.withReactSynchronous "app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run