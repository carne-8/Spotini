namespace Spotify.Auth

open System
open Elmish
open Spotify.Auth

module Cmds =
    open Electron

    let handleSpotifyUserLog () =
        Cmd.ofSub (fun dispatch ->
            electron.ipcRenderer.once ("spotify-user-logged", (fun _evt token ->
                token
                |> unbox
                |> Msg.UserLogged
                |> dispatch
            )) |> ignore
        )

    let requestAccessToken userToken =
        Cmd.ofSub (fun dispatch ->
            promise {
                let clientId = Env.get "CLIENT_ID"
                let clientSecret = Env.get "CLIENT_SECRET"

                let code =
                    sprintf "%s:%s" clientId clientSecret
                    |> Base64.toBase64String
                    |> sprintf "Basic %s"

                let! result = API.Spotify.Auth.getAccessToken userToken code

                result
                |> Msg.AccessTokenLoaded
                |> dispatch
            } |> Promise.start
        )

    let refreshAccessToken refreshToken =
        Cmd.ofSub (fun dispatch ->
            promise {
                let clientId = Env.get "CLIENT_ID"
                let clientSecret = Env.get "CLIENT_SECRET"

                let code =
                    sprintf "%s:%s" clientId clientSecret
                    |> Base64.toBase64String
                    |> sprintf "Basic %s"

                let! result =
                    refreshToken
                    |> API.Spotify.Auth.refreshAccessToken code

                (refreshToken, result)
                |> Msg.AccessTokenRefreshed
                |> dispatch
            } |> Promise.start
        )

module State =
    let initModel () =
        { UserToken = None
          AccessToken = None }, Cmds.handleSpotifyUserLog()

    let loadNewAccessToken refreshToken =
        { UserToken = None
          AccessToken = None },
        Cmd.batch [
            refreshToken
            |> Cmds.refreshAccessToken

            Cmds.handleSpotifyUserLog()
        ]

    let update msg model: Model * Cmd<Msg> * OutMsg option =
        match msg with
        | Msg.UserLogged token -> model, Cmds.requestAccessToken token, None
        | Msg.AccessTokenLoaded result ->
            match result with
            | Ok response ->
                let expireAt = DateTimeOffset.Now.AddSeconds(response.expires_in |> float).Subtract(TimeSpan(0, 1, 0))
                let outAccessToken =
                    { AccessToken = response.access_token
                      RefreshToken = response.refresh_token
                      ExpireAt = expireAt }

                { model with AccessToken = Some outAccessToken },
                Cmd.none, outAccessToken |> OutMsg.AccessTokenLoaded |> Some
            | Error error ->
                model, Cmd.none, error|> OutMsg.AccessTokenFailedToLoad |> Some
        | Msg.AccessTokenRefreshed (refreshToken, result) ->
            match result with
            | Ok response ->
                let expireAt = DateTimeOffset.Now.AddSeconds(response.expires_in |> float).Subtract(TimeSpan(0, 1, 0))
                let outAccessToken =
                    { AccessToken = response.access_token
                      RefreshToken = refreshToken
                      ExpireAt = expireAt }

                { model with AccessToken = Some outAccessToken },
                Cmd.none, outAccessToken |> OutMsg.AccessTokenRefreshed |> Some
            | Error error ->
                model, Cmd.none, error |> OutMsg.AccessTokenFailedToLoad |> Some