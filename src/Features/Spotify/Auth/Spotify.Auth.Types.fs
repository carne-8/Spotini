namespace Spotify.Auth

open System
open API.Spotify.Types.Auth

type AccessToken =
    { AccessToken: string
      RefreshToken: string
      ExpireAt: DateTimeOffset }

[<RequireQualifiedAccess>]
type Msg =
    | UserLogged of string
    | AccessTokenLoaded of Result<GetAccessTokenResponse, Thoth.Fetch.FetchError>
    | AccessTokenRefreshed of (string * Result<RefreshTokenResponse, Thoth.Fetch.FetchError>)

[<RequireQualifiedAccess>]
type OutMsg =
    | AccessTokenLoaded of AccessToken
    | AccessTokenRefreshed of AccessToken
    | AccessTokenFailedToLoad of Thoth.Fetch.FetchError

type Model =
    { UserToken: string option
      AccessToken: AccessToken option }