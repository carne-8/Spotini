namespace API.Spotify.Types

open Spotini
open Fable.Core

module Auth =
    type GetAccessTokenResponse =
        { access_token: string
          token_type: string
          expires_in: int
          refresh_token: string
          scope: string }

    type RefreshTokenResponse =
        { access_token: string
          token_type: string
          expires_in: int
          scope: string }

module Controller =
    type PlaySongBody =
        { uris: string list
          position_ms: int }

    type DevicesResult =
        { devices: Spotify.Device list }