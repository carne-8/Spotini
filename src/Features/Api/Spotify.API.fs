module API.Spotify

open Spotini
open Spotini.Spotify
open API.Spotify.Types
open Fable.Core
open Thoth.Fetch
open Fetch.Types

module Auth =
    open API.Spotify.Types.Auth

    let getAccessToken userToken code : JS.Promise<Result<GetAccessTokenResponse, FetchError>> =
        let headers =
            [ Authorization code
              ContentType "application/x-www-form-urlencoded" ]

        let url =
            "https://accounts.spotify.com/api/token" +
            "?grant_type=authorization_code" +
            "&code=" + userToken +
            "&redirect_uri=spotini://spotify-user-logged"

        Fetch.tryPost (url, headers = headers)

    let refreshAccessToken code refreshToken : JS.Promise<Result<RefreshTokenResponse, FetchError>> =
        let headers =
            [ ContentType "application/x-www-form-urlencoded"
              Authorization code ]

        let url =
            "https://accounts.spotify.com/api/token" +
            "?grant_type=refresh_token" +
            "&refresh_token=" + refreshToken

        Fetch.tryPost (url, headers = headers)

module Controller =
    open Thoth.Json
    open API.Spotify.Types.Controller

    let getDevices accessToken : JS.Promise<Result<DevicesResult, FetchError>> =
        let headers =
            [ ContentType "application/json"
              Authorization ("Bearer " + accessToken) ]

        let url = "https://api.spotify.com/v1/me/player/devices"

        Fetch.tryGet (url, headers = headers)

    let resumeSong deviceId accessToken : JS.Promise<Result<unit, FetchError>> =
        let headers =
            [ ContentType "application/json"
              Authorization ("Bearer " + accessToken) ]

        let url = "https://api.spotify.com/v1/me/player/play?device_id=" + deviceId

        Fetch.tryPut (url, headers = headers, decoder = Decode.unit)

    let pauseSong deviceId accessToken : JS.Promise<Result<unit, FetchError>> =
        let headers =
            [ ContentType "application/json"
              Authorization ("Bearer " + accessToken) ]

        let url = "https://api.spotify.com/v1/me/player/pause?device_id=" + deviceId

        Fetch.tryPut (url, headers = headers, decoder = Decode.unit)

    let getPlayer accessToken : JS.Promise<Result<Playback option, FetchError>> =
        promise {
            let headers = [ Authorization ("Bearer " + accessToken) ]

            let url = "https://api.spotify.com/v1/me/player"

            let! response =
                Helper.fetch
                    url
                    [ Method HttpMethod.GET
                      Fetch.requestHeaders headers ]


            let! result =
                match response.Status with
                | 200 ->
                    response.json()
                    |> Promise.map (fun body ->
                        body
                        |> JS.JSON.stringify
                        |> Json.decodeFromString
                        |> Result.map (fun playback ->
                            playback |> Some
                        )
                        |> Result.mapError (fun decodingError ->
                            decodingError
                            |> FetchError.DecodingFailed
                        )
                    )
                | 204 -> None |> Result.Ok |> Promise.lift
                | _ ->
                    response
                    |> FetchError.FetchFailed
                    |> Result.Error
                    |> Promise.lift

            return result
        }

    type SetDeviceBody =
        { device_ids: string list }

    let setDevice accessToken deviceId : JS.Promise<FetchError option> =
        let headers = [ Authorization ("Bearer " + accessToken) ]

        let url = "https://api.spotify.com/v1/me/player"

        let (body: SetDeviceBody) =
            { device_ids = [ deviceId ] }

        Fetch.tryPut (url, body, headers = headers, caseStrategy = CaseStrategy.SnakeCase, decoder = Decode.unit)
        |> Promise.map (fun result ->
            match result with
            | Ok _ -> None
            | Error error -> Some error
        )