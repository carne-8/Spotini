namespace Spotini

open Fable.Core

module Spotify =
    type Device =
        { id: string
          name: string }

    [<StringEnum>]
    type RepeatState =
        | [<CompiledName("off")>] Off
        | [<CompiledName("Context")>] Context
        | [<CompiledName("Track")>] Track

    type Image =
        { url: string }

    type Album =
        { name: string
          images: Image list }

    type Artist =
        { name: string
          external_urls: {| spotify: string |} }

    type Song =
        { name: string
          album: Album
          artists: Artist list }

    type Playback =
        { device: Device
          shuffle_state: bool
          repeat_state: RepeatState
          is_playing: bool
          item: Song option }