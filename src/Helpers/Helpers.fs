[<AutoOpen>]
module Spotini.Helpers


module React =
    open Feliz

    let useForceRerender () =
        let (value, setValue) = React.useState 0
        (fun () -> setValue 1)


module Json =
    open Thoth.Json
    open Thoth.Fetch
    open Fable.Core
    open Fable.Core.JsInterop

    let inline encoder<'T> = Encode.Auto.generateEncoderCached<'T> (caseStrategy = CaseStrategy.CamelCase)
    let inline decoder<'T> = Decode.Auto.generateDecoderCached<'T> (caseStrategy = CaseStrategy.CamelCase)

    let inline encoderPascal<'T> = Encode.Auto.generateEncoderCached<'T> (caseStrategy = CaseStrategy.PascalCase)
    let inline decoderPascal<'T> = Decode.Auto.generateDecoderCached<'T> (caseStrategy = CaseStrategy.PascalCase)


    let inline encodeToString<'T> = encoder<'T> >> Encode.toString 0
    let inline decodeFromString<'T> = decoder<'T> |> Decode.fromString