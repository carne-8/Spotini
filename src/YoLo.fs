[<AutoOpen>]
module YoLo

type Loadable<'a> =
    | Loading of 'a option
    | Loaded of 'a

module Loadable =
    let setLoading loadable =
        match loadable with
        | Loading _ -> loadable
        | Loaded teams -> Loading (Some teams)

    let setLoaded loadable =
        match loadable with
        | Loading loading -> loading |> Option.map Loaded
        | Loaded _ -> Some loadable

    let isLoading loadable =
        match loadable with
        | Loading _ -> true
        | Loaded _ -> false

module Env =
    open Fable.Core
    open Fable.Core.JsInterop

    [<Emit("process.env[$0] ? process.env[$0] : ''")>]
    let get (key: string) : string = jsNative

module Base64 =
    open System

    let toBase64String (str: string) : string =
        str
        |> Seq.map byte
        |> Array.ofSeq
        |> System.Convert.ToBase64String

    let fromBase64String str =
        str
        |> Convert.FromBase64String
        |> Text.Encoding.UTF8.GetString
