module App.View

open App
open Feliz
open Fable.Core
open Fable.Core.JsInterop
open Spotini.Assets
open Zanaptak.TypedCssClasses

importAll "./App.sass"
let crossIcon = importDefault "../assets/icons/cross.svg"
let pin = importDefault "../assets/icons/pin2.svg"
let pinFilled = importDefault "../assets/icons/pin2-filled.svg"

[<Literal>]
let ControllerSass = __SOURCE_DIRECTORY__ + "/App.sass"
type Style = CssClasses<ControllerSass, Naming.Verbatim, commandFile = "node", argumentPrefix = "../sass-process.js">


[<Erase>]
type prop =
    static member inline url (value: string) = Interop.mkAttr "url" value

let view (model: Model) dispatch =
    React.fragment [
        match model.Page with
        | Page.Loading ->
            Html.div [
                prop.style [
                    style.width 40
                    style.height 40
                    style.backgroundColor "red"
                ]
            ]
        | Page.Controller pageModel ->
            Pages.Controller.View.Controller
                {| model = pageModel
                   dispatch = (PageMessage.Controller >> Msg.PageMessage >> dispatch) |}
        | Page.Connect pageModel ->
            Pages.Connect.View.View pageModel (PageMessage.Connect >> Msg.PageMessage >> dispatch)

        Html.div [
            prop.className Style.``window-control-actions``
            prop.children [
                Html.i [
                    prop.classes [
                        Icons.``fi-rr-expand``
                        Style.``move-icon``
                    ]
                ]
                Html.i [
                    prop.classes [
                        Icons.``fi-rr-compress-alt``
                        Style.``line-icon``
                    ]
                    prop.onClick (fun _ ->
                        Msg.MinimizeApp
                        |> dispatch
                    )
                ]
                Html.img [
                    if model.AlwaysOnTop then
                        prop.src pinFilled
                    else prop.src pin
                    prop.className Style.``top-icon``
                    prop.onClick (fun _ -> Msg.ToggleAlwaysOnTop |> dispatch)
                ]
                Html.img [
                    prop.src crossIcon
                    prop.className Style.``cross-icon``
                    prop.onClick (fun _ ->
                        Msg.QuitApp
                        |> dispatch
                    )
                ]
            ]
        ]
    ]