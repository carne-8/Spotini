module App

open App
open System
open Elmish

open Fable.Core
open Fable.Core.JsInterop

[<ImportDefault("electron")>]
let electron: Electron.AllElectron = jsNative

module Cmds =
    let getAlwaysOnTopState () =
        Cmd.ofSub (fun dispatch ->
            electron.ipcRenderer.on("is-always-on-top", (fun _evt storedValue ->
                storedValue
                |> unbox
                |> Msg.CorrectAlwaysOnTop
                |> dispatch
            )) |> ignore

            electron.ipcRenderer.send("get-is-always-on-top")
        )

    let tryGetRefreshToken () =
        Cmd.ofSub (fun dispatch ->
            electron.ipcRenderer.on("refresh-token", (fun _evt refreshToken ->
                match refreshToken |> unbox with
                | Some encodedRefreshToken ->
                    encodedRefreshToken
                    |> Base64.fromBase64String
                    |> Msg.RefreshTokenFound
                    |> dispatch
                | None -> Msg.NoRefreshTokenFound |> dispatch
            )) |> ignore

            electron.ipcRenderer.send("get-refresh-token")
        )

module State =
    type SpotifyMsg = Spotify.Auth.OutMsg
    module SpotifyAuth = Spotify.Auth.State

    let init () =
        let model =
            { Page = Page.Loading
              SpotifyAuth = None
              AccessToken = None
              AuthError = None
              AlwaysOnTop = false }

        model,
        Cmd.batch [
          Cmds.getAlwaysOnTopState()
          Cmds.tryGetRefreshToken()
        ]

    // let init () =
    //     let model =
    //         { Page = Page.Controller { AccessToken = ""; FetchError = None; Devices = (Some [{ id = ""; name = "GAEL-WINDOWS" }]); SelectedDevice = None }
    //           SpotifyAuth = None
    //           AccessToken = None
    //           AuthError = None
    //           AlwaysOnTop = false }

    //     model,
    //     Cmds.getAlwaysOnTopState()

    let update msg model =
        match msg with
        | Msg.ToggleAlwaysOnTop ->
            electron.ipcRenderer.send "toggle-always-on-top" |> ignore
            { model with AlwaysOnTop = not model.AlwaysOnTop}, Cmd.none

        | Msg.CorrectAlwaysOnTop newValue ->
            { model with AlwaysOnTop = newValue}, Cmd.none

        | Msg.MinimizeApp ->
            electron.ipcRenderer.send "minimize" |> ignore
            model, Cmd.none

        | Msg.QuitApp ->
            electron.ipcRenderer.send "quit" |> ignore
            model, Cmd.none

        | Msg.NoRefreshTokenFound ->
            let (spotify, spotifyCmd) = SpotifyAuth.initModel()
            let (connectPage, connectPageCmd) = Pages.Connect.State.init None

            { model with
                Page = connectPage |> Page.Connect
                SpotifyAuth = Some spotify },
            Cmd.batch [
                spotifyCmd |> Cmd.map Msg.SpotifyMsg
                connectPageCmd |> Cmd.map (PageMessage.Connect >> Msg.PageMessage)
            ]

        | Msg.RefreshTokenFound refreshToken ->
            let (spotify, spotifyCmd) =
                refreshToken |> SpotifyAuth.loadNewAccessToken

            { model with SpotifyAuth = Some spotify },
            spotifyCmd |> Cmd.map Msg.SpotifyMsg

        | Msg.PageMessage pageMsg ->
            match model.Page, pageMsg with
            | Page.Connect pageModel, PageMessage.Connect msg ->
                let (connectPage, connectPageCmd) =
                    Pages.Connect.State.update msg pageModel

                { model with Page = connectPage |> Page.Connect },
                connectPageCmd |> Cmd.map (PageMessage.Connect >> Msg.PageMessage)

            | Page.Controller pageModel, PageMessage.Controller msg ->
                let (controllerPage, controllerPageCmd, controllerOutMsg) =
                    Pages.Controller.State.update msg pageModel

                match controllerOutMsg with
                | Some (Pages.Controller.OutMsg.RequestRefreshToken) ->
                    match model.AccessToken with
                    | Some ({ RefreshToken = refreshToken }) ->
                        let (spotify, spotifyCmd) =
                            refreshToken |> SpotifyAuth.loadNewAccessToken

                        { model with SpotifyAuth = Some spotify },
                        spotifyCmd |> Cmd.map Msg.SpotifyMsg
                    | None -> model, Cmd.none

                | None ->
                    { model with Page = controllerPage |> Page.Controller },
                    controllerPageCmd |> Cmd.map (PageMessage.Controller >> Msg.PageMessage)

            | _ -> model, Cmd.none

        | Msg.SpotifyMsg msg ->
            match model.SpotifyAuth with
            | Some spotifyAuth ->
                let (spotify, spotifyCmd, outMsgOption) = SpotifyAuth.update msg spotifyAuth

                match outMsgOption with
                | Some msg ->
                    match msg with
                    | SpotifyMsg.AccessTokenFailedToLoad error ->
                        let (connectPage, connectPageCmd) =
                            Pages.Connect.State.init (Some error)

                        { model with
                            Page = connectPage |> Page.Connect
                            SpotifyAuth = spotify |> Some
                            AuthError = Some error },
                        connectPageCmd |> Cmd.map (PageMessage.Connect >> Msg.PageMessage)

                    | SpotifyMsg.AccessTokenLoaded accessToken ->
                        let encodedRefreshToken = accessToken.RefreshToken |> Base64.toBase64String
                        electron.ipcRenderer.send("set-refresh-token", encodedRefreshToken)

                        let (controllerPage, controllerPageCmd) =
                            accessToken.AccessToken |> Pages.Controller.State.init

                        { model with
                            Page = controllerPage |> Page.Controller
                            SpotifyAuth = spotify |> Some
                            AccessToken = accessToken |> Some },
                        controllerPageCmd |> Cmd.map (PageMessage.Controller >> Msg.PageMessage)

                    | SpotifyMsg.AccessTokenRefreshed accessToken ->
                        let (controllerPage, controllerPageCmd) =
                            accessToken.AccessToken |> Pages.Controller.State.init

                        { model with
                            Page = controllerPage |> Page.Controller
                            SpotifyAuth = spotify |> Some
                            AccessToken = accessToken |> Some },
                        controllerPageCmd |> Cmd.map (PageMessage.Controller >> Msg.PageMessage)
                | None ->
                    { model with SpotifyAuth = spotify |> Some }, spotifyCmd |> Cmd.map Msg.SpotifyMsg
            | None -> model, Cmd.none