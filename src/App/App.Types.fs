namespace App

[<RequireQualifiedAccess>]
type PageMessage =
    | Connect of Pages.Connect.Msg
    | Controller of Pages.Controller.Msg

[<RequireQualifiedAccess>]
type Msg =
    | CorrectAlwaysOnTop of bool
    | ToggleAlwaysOnTop
    | MinimizeApp
    | QuitApp

    | RefreshTokenFound of string
    | NoRefreshTokenFound

    | SpotifyMsg of Spotify.Auth.Msg
    | PageMessage of PageMessage

[<RequireQualifiedAccess>]
type Page =
    | Loading
    | Connect of Pages.Connect.Model
    | Controller of Pages.Controller.Model


type Model =
    { Page: Page
      SpotifyAuth: Spotify.Auth.Model option
      AccessToken: Spotify.Auth.AccessToken option
      AuthError: Thoth.Fetch.FetchError option
      AlwaysOnTop: bool }